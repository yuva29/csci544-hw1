#!/usr/bin/python3

import sys

def countTokens(inputFile, outputFile):
	if(inputFile is None or outputFile is None):
		return
	try:
		inputFileHandler = open(inputFile, 'r')
		outputFileHandler = open(outputFile, 'w')
		for line in inputFileHandler:
			numTokens = len(line.split())
			outputFileHandler.write(str(numTokens)+'\n')
		inputFileHandler.close()
		outputFileHandler.close()
	except:
		print('ERROR: Provide valid filename')

argLen = len(sys.argv)
if(argLen <3 or argLen>3):
	print('ERROR: Provide valid arguments e.g python3 assignment1.py /path/to/inputfile /path/to/outputfile')
else:
	countTokens(sys.argv[1], sys.argv[2]) #argv[0] - program name
	
